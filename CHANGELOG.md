2020-10-13
   - 0-lepton plots without INTERNAL
   - new cover
   - corrections from defense report

2020-09-18
   - acknowledgements
   - new plots for defense

2020-09-13
   - Comments from Didier+Laurent
   - changes from 0l paper
   - public plots from HGTD TDR
   - updated cover

2020-08-18
   - Most comments from Isabelle

2020-07-15
   - first draft for the referees

2020-07-14
   - final corrections
   - final bibliography

2020-07-11
    - bibliography
    - corrections from Lydia and Laurent

2020-07-08
    - very few corrections on 0l + geometry chapter
    - all other chapters done
    - appendix with bestSRs of 0l
    - removed LQ appendix, moved discussion to main body

2020-06-27
    - Theory chapter done

2020-06-24
    - ATLAS chapter done
    - Theory chapter: SM + intro to susy
    - most of Laurent's correction on HGTD + Geometry chapters
 
2020-06-18
    - ATLAS chapter (finished reco + all ATLAS upgrades)

2020-06-17
    - ATLAS chapter (ATLAS detector + reco)

2020-06-15
    - ALTIROC v1 finalized
    - ATLAS chapter (LHC + ID)

2020-06-03
    - ALTIROC chapter being finalized

2020-05-22
    - 0-lepton 1st draft
    - all plots
    - hgtd + geometry chapters with 1st round corrections

2020-04-21
    - corrections of HGTD chapter - 1st final version
    - additional items in geometry and 0l chapters

2020-04-10
    - 0l chapter - statistics, fit and results

2020-03-21
    - 0l chapter - background estimation

2020-03-19
    - 0l chapter - event selection  

2020-03-17
    - 0l chapter - target models, analysis strategy and simulation
    - added .sty files for variable definitions

2020-03-12
    - HGTD chapter - physics and performance

2020-03-10
    - Geometry chapter - simplified framework, component optimization

2020-03-09
    - Geometry chapter - power, simplified framework

2020-03-06
    - Geometry chapter - occupancy, bandwidth

2020-03-05
    - Geometry chapter - simulation, occupancy

2020-03-04
    - ALTIROC chapter (alt1 tb)
    - Geometry introduction

2020-03-03
    - ALTIROC chapter
    - moved HL-LHC to ATLAS chapter

2020-02-12
    - Continued work on HGTD chapter (DAQ)

2020-02-03
    - Continued work on HGTD chapter (sensor + electronics)

2020-02-03
    - Continued work on HGTD chapter (layout + sensors)

2020-01-31
    - fixed channel numbering
    - new outline with seperate geometry chapter
