void hitsinasic1_Layer2_Quad0_Row6_Module12()
{
//=========Macro generated from canvas: c1/c1
//=========  (Fri Mar  6 15:39:22 2020) by ROOT version6.04/18
   TCanvas *c1 = new TCanvas("c1", "c1",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1->SetHighLightColor(2);
   c1->Range(-18.22785,-1.10633,95.6962,3.926798);
   c1->SetFillColor(0);
   c1->SetBorderMode(0);
   c1->SetBorderSize(2);
   c1->SetLogy();
   c1->SetTickx(1);
   c1->SetTicky(1);
   c1->SetLeftMargin(0.16);
   c1->SetRightMargin(0.05);
   c1->SetTopMargin(0.05);
   c1->SetBottomMargin(0.16);
   c1->SetFrameBorderMode(0);
   c1->SetFrameBorderMode(0);
   
   TH1F *hitsInAsic1_L2_Quad0_Row6_Mod12__125 = new TH1F("hitsInAsic1_L2_Quad0_Row6_Mod12__125","hitsInAsic1_L2_Quad0_Row6_Mod12",250,0,250);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetBinContent(1,1660);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetBinContent(2,2498);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetBinContent(3,2281);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetBinContent(4,1514);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetBinContent(5,969);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetBinContent(6,505);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetBinContent(7,252);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetBinContent(8,138);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetBinContent(9,110);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetBinContent(10,44);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetBinContent(11,20);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetBinContent(12,6);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetBinContent(13,2);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetBinContent(14,1);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetEntries(10000);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetStats(0);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetLineWidth(2);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetMarkerStyle(20);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->SetMarkerSize(1.2);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->GetXaxis()->SetTitle("Nb. of hit pads in ASIC");
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->GetXaxis()->SetRange(1,90);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->GetXaxis()->SetLabelFont(42);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->GetXaxis()->SetLabelSize(0.05);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->GetXaxis()->SetTitleSize(0.05);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->GetXaxis()->SetTitleOffset(1.4);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->GetXaxis()->SetTitleFont(42);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->GetYaxis()->SetLabelFont(42);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->GetYaxis()->SetLabelSize(0.05);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->GetYaxis()->SetTitleSize(0.05);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->GetYaxis()->SetTitleOffset(1.4);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->GetYaxis()->SetTitleFont(42);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->GetZaxis()->SetLabelFont(42);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->GetZaxis()->SetLabelSize(0.05);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->GetZaxis()->SetTitleSize(0.05);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->GetZaxis()->SetTitleFont(42);
   hitsInAsic1_L2_Quad0_Row6_Mod12__125->Draw("");
   TLatex *   tex = new TLatex(0.47,0.6,"ASIC R = 509.7 mm");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.47,0.54,"<hits> = 2.2");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetLineWidth(2);
   tex->Draw();
   c1->Modified();
   c1->cd();
   c1->SetSelected(c1);
}
