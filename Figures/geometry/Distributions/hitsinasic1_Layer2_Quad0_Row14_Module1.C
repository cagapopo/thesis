void hitsinasic1_Layer2_Quad0_Row14_Module1()
{
//=========Macro generated from canvas: c1/c1
//=========  (Fri Mar  6 15:39:23 2020) by ROOT version6.04/18
   TCanvas *c1 = new TCanvas("c1", "c1",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1->SetHighLightColor(2);
   c1->Range(-18.22785,-1.133188,95.6962,4.067798);
   c1->SetFillColor(0);
   c1->SetBorderMode(0);
   c1->SetBorderSize(2);
   c1->SetLogy();
   c1->SetTickx(1);
   c1->SetTicky(1);
   c1->SetLeftMargin(0.16);
   c1->SetRightMargin(0.05);
   c1->SetTopMargin(0.05);
   c1->SetBottomMargin(0.16);
   c1->SetFrameBorderMode(0);
   c1->SetFrameBorderMode(0);
   
   TH1F *hitsInAsic1_L2_Quad0_Row14_Mod1__213 = new TH1F("hitsInAsic1_L2_Quad0_Row14_Mod1__213","hitsInAsic1_L2_Quad0_Row14_Mod1",250,0,250);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->SetBinContent(1,3306);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->SetBinContent(2,3390);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->SetBinContent(3,1905);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->SetBinContent(4,880);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->SetBinContent(5,343);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->SetBinContent(6,120);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->SetBinContent(7,36);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->SetBinContent(8,16);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->SetBinContent(9,3);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->SetBinContent(10,1);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->SetEntries(10000);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->SetStats(0);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->SetLineWidth(2);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->SetMarkerStyle(20);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->SetMarkerSize(1.2);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->GetXaxis()->SetTitle("Nb. of hit pads in ASIC");
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->GetXaxis()->SetRange(1,90);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->GetXaxis()->SetLabelFont(42);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->GetXaxis()->SetLabelSize(0.05);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->GetXaxis()->SetTitleSize(0.05);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->GetXaxis()->SetTitleOffset(1.4);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->GetXaxis()->SetTitleFont(42);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->GetYaxis()->SetLabelFont(42);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->GetYaxis()->SetLabelSize(0.05);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->GetYaxis()->SetTitleSize(0.05);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->GetYaxis()->SetTitleOffset(1.4);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->GetYaxis()->SetTitleFont(42);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->GetZaxis()->SetLabelFont(42);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->GetZaxis()->SetLabelSize(0.05);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->GetZaxis()->SetTitleSize(0.05);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->GetZaxis()->SetTitleFont(42);
   hitsInAsic1_L2_Quad0_Row14_Mod1__213->Draw("");
   TLatex *   tex = new TLatex(0.47,0.6,"ASIC R = 598.6 mm");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.47,0.54,"<hits> = 1.2");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetLineWidth(2);
   tex->Draw();
   c1->Modified();
   c1->cd();
   c1->SetSelected(c1);
}
