void hitsinasic1_Layer2_Quad0_Row6_Module13()
{
//=========Macro generated from canvas: c1_n2/c1_n2
//=========  (Fri Mar  6 15:39:22 2020) by ROOT version6.04/18
   TCanvas *c1_n2 = new TCanvas("c1_n2", "c1_n2",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n2->SetHighLightColor(2);
   c1_n2->Range(-18.22785,-0.5425214,95.6962,3.948808);
   c1_n2->SetFillColor(0);
   c1_n2->SetBorderMode(0);
   c1_n2->SetBorderSize(2);
   c1_n2->SetLogy();
   c1_n2->SetTickx(1);
   c1_n2->SetTicky(1);
   c1_n2->SetLeftMargin(0.16);
   c1_n2->SetRightMargin(0.05);
   c1_n2->SetTopMargin(0.05);
   c1_n2->SetBottomMargin(0.16);
   c1_n2->SetFrameBorderMode(0);
   c1_n2->SetFrameBorderMode(0);
   
   TH1F *hitsInAsic1_L2_Quad0_Row6_Mod13__126 = new TH1F("hitsInAsic1_L2_Quad0_Row6_Mod13__126","hitsInAsic1_L2_Quad0_Row6_Mod13",250,0,250);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetBinContent(1,2066);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetBinContent(2,2797);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetBinContent(3,2260);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetBinContent(4,1454);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetBinContent(5,715);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetBinContent(6,374);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetBinContent(7,169);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetBinContent(8,88);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetBinContent(9,42);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetBinContent(10,22);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetBinContent(11,5);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetBinContent(12,3);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetBinContent(13,5);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetEntries(10000);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetStats(0);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetLineWidth(2);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetMarkerStyle(20);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->SetMarkerSize(1.2);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->GetXaxis()->SetTitle("Nb. of hit pads in ASIC");
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->GetXaxis()->SetRange(1,90);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->GetXaxis()->SetLabelFont(42);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->GetXaxis()->SetLabelSize(0.05);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->GetXaxis()->SetTitleSize(0.05);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->GetXaxis()->SetTitleOffset(1.4);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->GetXaxis()->SetTitleFont(42);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->GetYaxis()->SetLabelFont(42);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->GetYaxis()->SetLabelSize(0.05);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->GetYaxis()->SetTitleSize(0.05);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->GetYaxis()->SetTitleOffset(1.4);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->GetYaxis()->SetTitleFont(42);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->GetZaxis()->SetLabelFont(42);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->GetZaxis()->SetLabelSize(0.05);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->GetZaxis()->SetTitleSize(0.05);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->GetZaxis()->SetTitleFont(42);
   hitsInAsic1_L2_Quad0_Row6_Mod13__126->Draw("");
   TLatex *   tex = new TLatex(0.47,0.6,"ASIC R = 539.8 mm");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.47,0.54,"<hits> = 1.9");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetLineWidth(2);
   tex->Draw();
   c1_n2->Modified();
   c1_n2->cd();
   c1_n2->SetSelected(c1_n2);
}
