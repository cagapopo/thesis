void occupancy_mb200_2hits()
{
//=========Macro generated from canvas: c1/c1
//=========  (Fri Mar  6 13:28:23 2020) by ROOT version6.04/14
   TCanvas *c1 = new TCanvas("c1", "c1",73,52,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1->SetHighLightColor(2);
   c1->Range(22.78481,-1.528453,630.3797,1.774376);
   c1->SetFillColor(0);
   c1->SetBorderMode(0);
   c1->SetBorderSize(2);
   c1->SetLogy();
   c1->SetTicky(1);
   c1->SetLeftMargin(0.16);
   c1->SetRightMargin(0.05);
   c1->SetBottomMargin(0.16);
   c1->SetFrameBorderMode(0);
   c1->SetFrameBorderMode(0);
   
   TH1D *Nb_cells_2hits_Layer__4__1 = new TH1D("Nb_cells_2hits_Layer__4__1","Nb_cells_2hits_Layer__4",20,120,600);
   Nb_cells_2hits_Layer__4__1->SetBinContent(1,9.329483);
   Nb_cells_2hits_Layer__4__1->SetBinContent(2,8.74397);
   Nb_cells_2hits_Layer__4__1->SetBinContent(3,7.917426);
   Nb_cells_2hits_Layer__4__1->SetBinContent(4,7.305828);
   Nb_cells_2hits_Layer__4__1->SetBinContent(5,6.907068);
   Nb_cells_2hits_Layer__4__1->SetBinContent(6,6.491157);
   Nb_cells_2hits_Layer__4__1->SetBinContent(7,6.141278);
   Nb_cells_2hits_Layer__4__1->SetBinContent(8,5.94743);
   Nb_cells_2hits_Layer__4__1->SetBinContent(9,5.745072);
   Nb_cells_2hits_Layer__4__1->SetBinContent(10,5.590939);
   Nb_cells_2hits_Layer__4__1->SetBinContent(11,5.427931);
   Nb_cells_2hits_Layer__4__1->SetBinContent(12,5.101802);
   Nb_cells_2hits_Layer__4__1->SetBinContent(13,4.894359);
   Nb_cells_2hits_Layer__4__1->SetBinContent(14,4.837559);
   Nb_cells_2hits_Layer__4__1->SetBinContent(15,4.768474);
   Nb_cells_2hits_Layer__4__1->SetBinContent(16,4.724774);
   Nb_cells_2hits_Layer__4__1->SetBinContent(17,4.681876);
   Nb_cells_2hits_Layer__4__1->SetBinContent(18,4.622104);
   Nb_cells_2hits_Layer__4__1->SetBinContent(19,4.595079);
   Nb_cells_2hits_Layer__4__1->SetBinContent(20,4.443115);
   Nb_cells_2hits_Layer__4__1->SetMinimum(0.1);
   Nb_cells_2hits_Layer__4__1->SetMaximum(30);
   Nb_cells_2hits_Layer__4__1->SetEntries(1.182167);
   Nb_cells_2hits_Layer__4__1->SetStats(0);
   Nb_cells_2hits_Layer__4__1->SetLineWidth(2);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#0000ff");
   Nb_cells_2hits_Layer__4__1->SetMarkerColor(ci);
   Nb_cells_2hits_Layer__4__1->SetMarkerStyle(20);
   Nb_cells_2hits_Layer__4__1->SetMarkerSize(1.2);
   Nb_cells_2hits_Layer__4__1->GetXaxis()->SetTitle("Radius [mm]");
   Nb_cells_2hits_Layer__4__1->GetXaxis()->SetLabelFont(42);
   Nb_cells_2hits_Layer__4__1->GetXaxis()->SetLabelSize(0.05);
   Nb_cells_2hits_Layer__4__1->GetXaxis()->SetTitleSize(0.05);
   Nb_cells_2hits_Layer__4__1->GetXaxis()->SetTitleOffset(1.4);
   Nb_cells_2hits_Layer__4__1->GetXaxis()->SetTitleFont(42);
   Nb_cells_2hits_Layer__4__1->GetYaxis()->SetTitle("Fraction of pads with >= 2 hits [%]");
   Nb_cells_2hits_Layer__4__1->GetYaxis()->SetMoreLogLabels();
   Nb_cells_2hits_Layer__4__1->GetYaxis()->SetLabelFont(42);
   Nb_cells_2hits_Layer__4__1->GetYaxis()->SetLabelSize(0.05);
   Nb_cells_2hits_Layer__4__1->GetYaxis()->SetTitleSize(0.05);
   Nb_cells_2hits_Layer__4__1->GetYaxis()->SetTitleOffset(1.4);
   Nb_cells_2hits_Layer__4__1->GetYaxis()->SetTitleFont(42);
   Nb_cells_2hits_Layer__4__1->GetZaxis()->SetLabelFont(42);
   Nb_cells_2hits_Layer__4__1->GetZaxis()->SetLabelSize(0.05);
   Nb_cells_2hits_Layer__4__1->GetZaxis()->SetTitleSize(0.05);
   Nb_cells_2hits_Layer__4__1->GetZaxis()->SetTitleFont(42);
   Nb_cells_2hits_Layer__4__1->Draw("P");
   
   TH1D *Nb_cells_dt_Layer__4__2 = new TH1D("Nb_cells_dt_Layer__4__2","Nb_cells_dt_Layer__4",20,120,600);
   Nb_cells_dt_Layer__4__2->SetBinContent(1,3.329618);
   Nb_cells_dt_Layer__4__2->SetBinContent(2,2.773822);
   Nb_cells_dt_Layer__4__2->SetBinContent(3,2.209412);
   Nb_cells_dt_Layer__4__2->SetBinContent(4,1.766364);
   Nb_cells_dt_Layer__4__2->SetBinContent(5,1.428741);
   Nb_cells_dt_Layer__4__2->SetBinContent(6,1.158059);
   Nb_cells_dt_Layer__4__2->SetBinContent(7,0.9368622);
   Nb_cells_dt_Layer__4__2->SetBinContent(8,0.7850331);
   Nb_cells_dt_Layer__4__2->SetBinContent(9,0.6669524);
   Nb_cells_dt_Layer__4__2->SetBinContent(10,0.5610223);
   Nb_cells_dt_Layer__4__2->SetBinContent(11,0.4699561);
   Nb_cells_dt_Layer__4__2->SetBinContent(12,0.396613);
   Nb_cells_dt_Layer__4__2->SetBinContent(13,0.3065364);
   Nb_cells_dt_Layer__4__2->SetBinContent(14,0.2922556);
   Nb_cells_dt_Layer__4__2->SetBinContent(15,0.2557585);
   Nb_cells_dt_Layer__4__2->SetBinContent(16,0.2150398);
   Nb_cells_dt_Layer__4__2->SetBinContent(17,0.1944622);
   Nb_cells_dt_Layer__4__2->SetBinContent(18,0.1760843);
   Nb_cells_dt_Layer__4__2->SetBinContent(19,0.1649769);
   Nb_cells_dt_Layer__4__2->SetBinContent(20,0.1513401);
   Nb_cells_dt_Layer__4__2->SetEntries(0.1823891);
   Nb_cells_dt_Layer__4__2->SetStats(0);
   Nb_cells_dt_Layer__4__2->SetLineWidth(2);

   ci = TColor::GetColor("#0000ff");
   Nb_cells_dt_Layer__4__2->SetMarkerColor(ci);
   Nb_cells_dt_Layer__4__2->SetMarkerStyle(26);
   Nb_cells_dt_Layer__4__2->SetMarkerSize(1.2);
   Nb_cells_dt_Layer__4__2->GetXaxis()->SetLabelFont(42);
   Nb_cells_dt_Layer__4__2->GetXaxis()->SetLabelSize(0.05);
   Nb_cells_dt_Layer__4__2->GetXaxis()->SetTitleSize(0.05);
   Nb_cells_dt_Layer__4__2->GetXaxis()->SetTitleOffset(1.4);
   Nb_cells_dt_Layer__4__2->GetXaxis()->SetTitleFont(42);
   Nb_cells_dt_Layer__4__2->GetYaxis()->SetLabelFont(42);
   Nb_cells_dt_Layer__4__2->GetYaxis()->SetLabelSize(0.05);
   Nb_cells_dt_Layer__4__2->GetYaxis()->SetTitleSize(0.05);
   Nb_cells_dt_Layer__4__2->GetYaxis()->SetTitleOffset(1.4);
   Nb_cells_dt_Layer__4__2->GetYaxis()->SetTitleFont(42);
   Nb_cells_dt_Layer__4__2->GetZaxis()->SetLabelFont(42);
   Nb_cells_dt_Layer__4__2->GetZaxis()->SetLabelSize(0.05);
   Nb_cells_dt_Layer__4__2->GetZaxis()->SetTitleSize(0.05);
   Nb_cells_dt_Layer__4__2->GetZaxis()->SetTitleFont(42);
   Nb_cells_dt_Layer__4__2->Draw("P same");
   
   TLegend *leg = new TLegend(0.7,0.7,0.89,0.89,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("Nb_cells_2hits_Layer__4","No time cut","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);

   ci = TColor::GetColor("#0000ff");
   entry->SetMarkerColor(ci);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1.2);
   entry->SetTextFont(62);
   entry=leg->AddEntry("Nb_cells_dt_Layer__4","max(dt)>20 ps","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);

   ci = TColor::GetColor("#0000ff");
   entry->SetMarkerColor(ci);
   entry->SetMarkerStyle(26);
   entry->SetMarkerSize(1.2);
   entry->SetTextFont(62);
   leg->Draw();
   TGaxis *gaxis = new TGaxis(120,30,600,30,2.458072,4.060453,505,"-");
   gaxis->SetLabelOffset(0.005);
   gaxis->SetLabelSize(0.04);
   gaxis->SetTickSize(0.03);
   gaxis->SetGridLength(0);
   gaxis->SetTitleOffset(0.8);
   gaxis->SetTitleSize(0.05);
   gaxis->SetTitleColor(1);
   gaxis->SetTitleFont(62);
   gaxis->SetTitle("#eta");
   gaxis->SetMoreLogLabels();
   gaxis->Draw();
   c1->Modified();
   c1->cd();
   c1->SetSelected(c1);
}
