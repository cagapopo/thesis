

\begin{table}
\begin{center}
\setlength{\tabcolsep}{0.0pc}
{\footnotesize
%%
\begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}lrrr}
\noalign{\smallskip}\hline\noalign{\smallskip}
{\textbf{MB-GGo}}           & VR0LdPhi            & VR0LmetSig\_10            & VR0LmetSig\_16              \\[-0.05cm]
\noalign{\smallskip}\hline\noalign{\smallskip}
%%
Observed          & $23$              & $65$              & $2$                    \\
\noalign{\smallskip}\hline\noalign{\smallskip}
%%
Fitted         & $20.6 \pm 2.4$          & $53.5 \pm 5.7$          & $1.6 \pm 0.5$              \\
\noalign{\smallskip}\hline\noalign{\smallskip}
%%
         Fitted Multijets & $0.1_{-0.1}^{+0.1}$          & $0.5_{-0.5}^{+0.5}$          & $0.0 \pm 0.0$              \\
%%
         Fitted Wjets & $3.3 \pm 1.1$          & $6.5 \pm 2.0$          & $0.6 \pm 0.2$              \\
%%
         Fitted Zjets & $2.7 \pm 1.3$          & $3.8 \pm 0.8$          & $0.2 \pm 0.1$              \\
%%
         Fitted GAMMAjets & $0.0 \pm 0.0$          & $0.0 \pm 0.0$          & $0.0 \pm 0.0$              \\
%%
         Fitted Top & $13.5 \pm 3.0$          & $40.4 \pm 5.7$          & $0.6 \pm 0.3$              \\
%%
         Fitted Diboson & $1.0 \pm 0.7$          & $2.3 \pm 1.0$          & $0.1 \pm 0.0$              \\
%%     
 \noalign{\smallskip}\hline\noalign{\smallskip}
%%
MC exp.              & $24.2$          & $61.1$          & $2.0$              \\
\noalign{\smallskip}\hline\noalign{\smallskip}
%%
         MC Multijets & $0.1$          & $0.5$          & $0.0$              \\
%%
         MC Wjets & $4.1$          & $8.8$          & $0.8$              \\
%%
         MC Zjets & $4.4$          & $5.9$          & $0.4$              \\
%%
         MC GAMMAjets & $0.0$          & $0.0$          & $0.0$              \\
%%
         MC Top & $14.6$          & $43.7$          & $0.7$              \\
%%
         MC Diboson & $0.9$          & $2.2$          & $0.1$              \\
%%     \\
\noalign{\smallskip}\hline\noalign{\smallskip}
\end{tabular*}
%%%
}
\end{center}
\caption{{\textbf{MB-GGo-6-1000}} : Background fit results for the VR0LdPhi-6-1000, VR0LmetSig-6-1000-10 and VR0LmetSig-6-1000-16 regions, for an integrated luminosity of 139.0 fb$^{-1}$. Nominal MC expectations (normalised to MC cross-sections) are given for comparison. The errors shown are the statistical plus systematic uncertainties. The errors shown for the signal region are systematic uncertainties only.}
\label{table.results.systematics.in.logL.fit.VR0LdPhi-6-1000.VR0LmetSig-6-1000-10.VR0LmetSig-6-1000-16.MB-GGo_6_1000}
\end{table}
%