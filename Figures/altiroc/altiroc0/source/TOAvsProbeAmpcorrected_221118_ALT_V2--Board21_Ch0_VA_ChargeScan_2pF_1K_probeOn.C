void TOAvsProbeAmpcorrected_221118_ALT_V2--Board21_Ch0_VA_ChargeScan_2pF_1K_probeOn()
{
//=========Macro generated from canvas: c1_n2/c1_n2
//=========  (Thu Nov 22 17:33:26 2018) by ROOT version 6.14/04
   TCanvas *c1_n2 = new TCanvas("c1_n2", "c1_n2",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n2->SetHighLightColor(2);
   c1_n2->Range(-29.76411,-17.36044,232.2763,15.08861);
   c1_n2->SetFillColor(0);
   c1_n2->SetBorderMode(0);
   c1_n2->SetBorderSize(2);
   c1_n2->SetGridx();
   c1_n2->SetGridy();
   c1_n2->SetTickx(1);
   c1_n2->SetTicky(1);
   c1_n2->SetLeftMargin(0.16);
   c1_n2->SetRightMargin(0.05);
   c1_n2->SetTopMargin(0.05);
   c1_n2->SetBottomMargin(0.16);
   c1_n2->SetFrameBorderMode(0);
   c1_n2->SetFrameBorderMode(0);
   
   Double_t Graph0_fx1005[10] = {
   29.43629,
   37.42975,
   48.16433,
   61.11119,
   77.96687,
   97.28694,
   120.2123,
   145.8111,
   174.5311,
   201.8828};
   Double_t Graph0_fy1005[10] = {
   -8.48202,
   10.62002,
   2.711349,
   -6.082605,
   -9.622914,
   5.186755,
   6.494339,
   -0.8913131,
   -0.185433,
   -1.27716};
   Double_t Graph0_fex1005[10] = {
   0.02294093,
   0.02455773,
   0.02590703,
   0.02676579,
   0.02924501,
   0.02854451,
   0.0273336,
   0.03611995,
   0.03596076,
   0.04047143};
   Double_t Graph0_fey1005[10] = {
   0.9545844,
   0.7099085,
   0.5761528,
   0.4761839,
   0.4094513,
   0.3648382,
   0.3125682,
   0.2788678,
   0.2440336,
   0.2128057};
   TGraphErrors *gre = new TGraphErrors(10,Graph0_fx1005,Graph0_fy1005,Graph0_fex1005,Graph0_fey1005);
   gre->SetName("Graph0");
   gre->SetTitle("TOAvsProbeAmpcorrected_221118_ALT_V2--Board21_Ch0_VA_ChargeScan_2pF_1K_probeOn");
   gre->SetFillStyle(1000);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#0000ff");
   gre->SetLineColor(ci);
   gre->SetLineWidth(3);

   ci = TColor::GetColor("#0000ff");
   gre->SetMarkerColor(ci);
   gre->SetMarkerStyle(20);
   gre->SetMarkerSize(1.4);
   
   TH1F *Graph_Graph01005 = new TH1F("Graph_Graph01005","TOAvsProbeAmpcorrected_221118_ALT_V2--Board21_Ch0_VA_ChargeScan_2pF_1K_probeOn",100,12.16235,219.1743);
   Graph_Graph01005->SetMinimum(-12.1686);
   Graph_Graph01005->SetMaximum(13.46616);
   Graph_Graph01005->SetDirectory(0);
   Graph_Graph01005->SetStats(0);
   Graph_Graph01005->SetLineWidth(2);
   Graph_Graph01005->SetMarkerStyle(20);
   Graph_Graph01005->SetMarkerSize(1.2);
   Graph_Graph01005->GetXaxis()->SetTitle("probe Amp [mV]");
   Graph_Graph01005->GetXaxis()->SetMoreLogLabels();
   Graph_Graph01005->GetXaxis()->SetLabelFont(42);
   Graph_Graph01005->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph01005->GetXaxis()->SetTitleSize(0.05);
   Graph_Graph01005->GetXaxis()->SetTitleOffset(1.4);
   Graph_Graph01005->GetXaxis()->SetTitleFont(42);
   Graph_Graph01005->GetYaxis()->SetTitle("Residual [ps]");
   Graph_Graph01005->GetYaxis()->SetLabelFont(42);
   Graph_Graph01005->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph01005->GetYaxis()->SetTitleSize(0.05);
   Graph_Graph01005->GetYaxis()->SetTitleOffset(1.4);
   Graph_Graph01005->GetYaxis()->SetTitleFont(42);
   Graph_Graph01005->GetZaxis()->SetLabelFont(42);
   Graph_Graph01005->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph01005->GetZaxis()->SetTitleSize(0.05);
   Graph_Graph01005->GetZaxis()->SetTitleFont(42);
   gre->SetHistogram(Graph_Graph01005);
   
   gre->Draw("ap");
   c1_n2->Modified();
   c1_n2->cd();
   c1_n2->SetSelected(c1_n2);
}
