void TOAvsTOTcorrected_221118_ALT_V2__Board21_Ch0_VA_ChargeScan_3pF_1K_probeOff()
{
//=========Macro generated from canvas: c1_n2/c1_n2
//=========  (Thu Nov 22 18:32:02 2018) by ROOT version 6.14/04
   //TCanvas *c1_n2 = new TCanvas("c1_n2", "c1_n2",0,0,700,500);
   //gStyle->SetOptStat(0);
   //gStyle->SetOptTitle(0);
   //c1_n2->SetHighLightColor(2);
   //c1_n2->Range(2025.485,-35.45322,11912.78,24.43548);
   //c1_n2->SetFillColor(0);
   //c1_n2->SetBorderMode(0);
   //c1_n2->SetBorderSize(2);
   //c1_n2->SetTickx(1);
   //c1_n2->SetTicky(1);
   //c1_n2->SetLeftMargin(0.16);
   //c1_n2->SetRightMargin(0.05);
   //c1_n2->SetTopMargin(0.05);
   //c1_n2->SetBottomMargin(0.16);
   //c1_n2->SetFrameBorderMode(0);
   //c1_n2->SetFrameBorderMode(0);
   
   Double_t Graph0_fx1005[10] = {
   4266.537,
   4954.588,
   5604.14,
   6253.307,
   7031.42,
   8020.125,
   8855.617,
   9680.113,
   10258.02,
   10762.18};
   Double_t Graph0_fy1005[10] = {
   16.33503,
   10.30887,
   0.6101419,
   -14.20299,
   -21.47075,
   8.103024,
   6.536321,
   3.210325,
   0.759418,
   -3.60131};
   Double_t Graph0_fex1005[10] = {
   8.170895,
   5.838862,
   5.673534,
   5.19447,
   8.75253,
   7.383633,
   7.372135,
   6.001645,
   4.671874,
   5.32407};
   Double_t Graph0_fey1005[10] = {
   1.163346,
   0.8931102,
   0.7263523,
   0.5853884,
   0.4576031,
   0.3879116,
   0.3198627,
   0.2846237,
   0.2512037,
   0.244412};
   TGraphErrors *gre = new TGraphErrors(10,Graph0_fx1005,Graph0_fy1005,Graph0_fex1005,Graph0_fey1005);
   gre->SetName("Graph0");
   gre->SetTitle("TOAvsTOTcorrected_221118_ALT_V2--Board21_Ch0_VA_ChargeScan_3pF_1K_probeOff");
   gre->SetFillStyle(1000);
   gre->SetLineWidth(3);
   gre->SetMarkerStyle(20);
   gre->SetMarkerSize(1.4);
   
   TH1F *Graph_Graph01005 = new TH1F("Graph_Graph01005","",100,3607.452,11418.42);
   Graph_Graph01005->SetMinimum(-25.87103);
   Graph_Graph01005->SetMaximum(21.44105);
   Graph_Graph01005->SetDirectory(0);
   Graph_Graph01005->SetStats(0);
   Graph_Graph01005->SetLineWidth(2);
   Graph_Graph01005->SetMarkerStyle(20);
   Graph_Graph01005->SetMarkerSize(1.2);
   Graph_Graph01005->GetXaxis()->SetTitle("TOT [ps]");
   Graph_Graph01005->GetXaxis()->SetMoreLogLabels();
   Graph_Graph01005->GetXaxis()->SetLabelFont(42);
   Graph_Graph01005->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph01005->GetXaxis()->SetTitleSize(0.07);
   Graph_Graph01005->GetXaxis()->SetTitleOffset(1.4);
   Graph_Graph01005->GetXaxis()->SetTitleFont(42);
   Graph_Graph01005->GetYaxis()->SetTitle("Residual [ps]");
   Graph_Graph01005->GetYaxis()->SetLabelFont(42);
   Graph_Graph01005->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph01005->GetYaxis()->SetTitleSize(0.07);
   Graph_Graph01005->GetYaxis()->SetTitleOffset(0.4);
   Graph_Graph01005->GetYaxis()->SetTitleFont(42);
   Graph_Graph01005->GetZaxis()->SetLabelFont(42);
   Graph_Graph01005->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph01005->GetZaxis()->SetTitleSize(0.05);
   Graph_Graph01005->GetZaxis()->SetTitleFont(42);
   gre->SetHistogram(Graph_Graph01005);
   
   gre->Draw("ap");

   //c1_n2->Modified();
   //c1_n2->cd();
   //c1_n2->SetSelected(c1_n2);
}
