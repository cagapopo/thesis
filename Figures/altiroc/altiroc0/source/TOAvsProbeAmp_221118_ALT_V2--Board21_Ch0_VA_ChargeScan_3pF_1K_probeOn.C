void TOAvsProbeAmp_221118_ALT_V2__Board21_Ch0_VA_ChargeScan_3pF_1K_probeOn()
{
//=========Macro generated from canvas: c1_n2/c1_n2
//=========  (Thu Nov 22 18:33:10 2018) by ROOT version 6.14/04
   //TCanvas *c1_n2 = new TCanvas("c1_n2", "c1_n2",0,0,700,500);
   //gStyle->SetOptStat(0);
   //gStyle->SetOptTitle(0);
   //c1_n2->SetHighLightColor(2);
   //c1_n2->Range(-28.59285,11637.25,216.3583,12573.46);
   //c1_n2->SetFillColor(0);
   //c1_n2->SetBorderMode(0);
   //c1_n2->SetBorderSize(2);
   //c1_n2->SetTickx(1);
   //c1_n2->SetTicky(1);
   //c1_n2->SetLeftMargin(0.16);
   //c1_n2->SetRightMargin(0.05);
   //c1_n2->SetTopMargin(0.05);
   //c1_n2->SetBottomMargin(0.16);
   //c1_n2->SetFrameBorderMode(0);
   //c1_n2->SetFrameBorderMode(0);
   
   Double_t Graph0_fx1002[10] = {
   26.75104,
   33.55751,
   43.27717,
   54.47701,
   69.18887,
   86.5666,
   107.0372,
   129.4977,
   158.7137,
   187.95};
   Double_t Graph0_fy1002[10] = {
   12463.75,
   12321,
   12200.03,
   12126.54,
   12043.79,
   12001.14,
   11946.7,
   11902.34,
   11872.55,
   11848.93};
   Double_t Graph0_fex1002[10] = {
   0.02574835,
   0.02595358,
   0.02544125,
   0.02620097,
   0.03023185,
   0.02977807,
   0.0274443,
   0.03823735,
   0.03528587,
   0.03483262};
   Double_t Graph0_fey1002[10] = {
   1.262072,
   0.954126,
   0.7834862,
   0.6503403,
   0.5462108,
   0.4612662,
   0.3811317,
   0.3149872,
   0.2760349,
   0.2529144};
   TGraphErrors *gre = new TGraphErrors(10,Graph0_fx1002,Graph0_fy1002,Graph0_fex1002,Graph0_fey1002);
   gre->SetName("Graph0");
   gre->SetTitle("TOAvsProbeAmp_221118_ALT_V2--Board21_Ch0_VA_ChargeScan_3pF_1K_probeOn");
   gre->SetFillStyle(1000);
   gre->SetLineWidth(3);
   gre->SetMarkerStyle(20);
   gre->SetMarkerSize(1.4);
   
   TH1F *Graph_Graph01002 = new TH1F("Graph_Graph01002","TOAvsProbeAmp_221118_ALT_V2--Board21_Ch0_VA_ChargeScan_3pF_1K_probeOn",100,10.59934,204.1108);
   Graph_Graph01002->SetMinimum(11787.04);
   Graph_Graph01002->SetMaximum(12526.65);
   Graph_Graph01002->SetDirectory(0);
   Graph_Graph01002->SetStats(0);
   Graph_Graph01002->SetLineWidth(2);
   Graph_Graph01002->SetMarkerStyle(20);
   Graph_Graph01002->SetMarkerSize(1.2);
   Graph_Graph01002->GetXaxis()->SetTitle("probe Amp [mV]");
   Graph_Graph01002->GetXaxis()->SetMoreLogLabels();
   Graph_Graph01002->GetXaxis()->SetLabelFont(42);
   Graph_Graph01002->GetXaxis()->SetLabelSize(0.05);
   Graph_Graph01002->GetXaxis()->SetTitleSize(0.05);
   Graph_Graph01002->GetXaxis()->SetTitleOffset(1.4);
   Graph_Graph01002->GetXaxis()->SetTitleFont(42);
   Graph_Graph01002->GetYaxis()->SetTitle("TOA[ps]");
   Graph_Graph01002->GetYaxis()->SetLabelFont(42);
   Graph_Graph01002->GetYaxis()->SetLabelSize(0.05);
   Graph_Graph01002->GetYaxis()->SetTitleSize(0.05);
   Graph_Graph01002->GetYaxis()->SetTitleOffset(1.4);
   Graph_Graph01002->GetYaxis()->SetTitleFont(42);
   Graph_Graph01002->GetZaxis()->SetLabelFont(42);
   Graph_Graph01002->GetZaxis()->SetLabelSize(0.05);
   Graph_Graph01002->GetZaxis()->SetTitleSize(0.05);
   Graph_Graph01002->GetZaxis()->SetTitleFont(42);
   gre->SetHistogram(Graph_Graph01002);
   
   
   TF1 *myfitProbe1003 = new TF1("myfitProbe","[0]/x+[1]/x^2+[2]/x^3+[3]",10.59934,204.1108, TF1::EAddToList::kNo);
   myfitProbe1003->SetFillColor(19);
   myfitProbe1003->SetFillStyle(0);
   myfitProbe1003->SetMarkerStyle(20);
   myfitProbe1003->SetMarkerSize(1.2);
   myfitProbe1003->SetLineWidth(3);
   myfitProbe1003->SetChisquare(844.419);
   myfitProbe1003->SetNDF(6);
   myfitProbe1003->GetXaxis()->SetLabelFont(42);
   myfitProbe1003->GetXaxis()->SetLabelSize(0.05);
   myfitProbe1003->GetXaxis()->SetTitleSize(0.05);
   myfitProbe1003->GetXaxis()->SetTitleOffset(1.4);
   myfitProbe1003->GetXaxis()->SetTitleFont(42);
   myfitProbe1003->GetYaxis()->SetLabelFont(42);
   myfitProbe1003->GetYaxis()->SetLabelSize(0.05);
   myfitProbe1003->GetYaxis()->SetTitleSize(0.05);
   myfitProbe1003->GetYaxis()->SetTitleOffset(1.4);
   myfitProbe1003->GetYaxis()->SetTitleFont(42);
   myfitProbe1003->SetParameter(0,31916.88);
   myfitProbe1003->SetParError(0,256.682);
   myfitProbe1003->SetParLimits(0,0,0);
   myfitProbe1003->SetParameter(1,-625223.4);
   myfitProbe1003->SetParError(1,16236.97);
   myfitProbe1003->SetParLimits(1,0,0);
   myfitProbe1003->SetParameter(2,8627761);
   myfitProbe1003->SetParError(2,288967.5);
   myfitProbe1003->SetParLimits(2,0,0);
   myfitProbe1003->SetParameter(3,11694.23);
   myfitProbe1003->SetParError(3,1.096812);
   myfitProbe1003->SetParLimits(3,0,0);
   myfitProbe1003->SetParent(gre);
   gre->GetListOfFunctions()->Add(myfitProbe1003);
   gre->Draw("ap");
   //c1_n2->Modified();
   //c1_n2->cd();
   //c1_n2->SetSelected(c1_n2);
}
