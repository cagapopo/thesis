void B13_ch0_thresScan_TOTmean_()
{
//=========Macro generated from canvas: c1_n5/c1_n5
//=========  (Thu May 28 19:09:46 2020) by ROOT version 6.12/06
   TCanvas *c1_n5 = new TCanvas("c1_n5", "c1_n5",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n5->SetHighLightColor(2);
   c1_n5->Range(0,0,1,1);
   c1_n5->SetFillColor(0);
   c1_n5->SetBorderMode(0);
   c1_n5->SetBorderSize(2);
   c1_n5->SetTickx(1);
   c1_n5->SetTicky(1);
   c1_n5->SetLeftMargin(0.16);
   c1_n5->SetRightMargin(0.28);
   c1_n5->SetTopMargin(0.05);
   c1_n5->SetBottomMargin(0.16);
   c1_n5->SetFrameBorderMode(0);
   
   TLegend *leg = new TLegend(0.7,0.4,0.99,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.05);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   leg->Draw();
   TLatex *   tex = new TLatex(0.2,0.85,"#bf{Testbench} measurement");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.77,"DUT ALT1-2");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetLineWidth(2);
   tex->Draw();
   c1_n5->Modified();
   c1_n5->cd();
   c1_n5->SetSelected(c1_n5);
}
